#include<iostream>
#include<iomanip>

using namespace std;

int BoardInit(int [6][7]);
void Display(int [6][7]);
int PlayerInput();
void GridUpdate(int [6][7] , int , int);
int turn(int &);
int Victory_Detect(int [6][7]);
void Winner_declaration(int );

int main(){
	int player_input;
	int turn_counter = 0; //for detecting P1 or P2 turn
	int grid[6][7];
	int lol = 0;
	int victory;
	BoardInit(grid);
	Display(grid);
	
	do{
	turn_counter++; //
	cout << "Player " << turn(turn_counter) <<"'s turn. " ;
	GridUpdate(grid , PlayerInput() , turn(turn_counter));
	Display(grid);
	cout << turn_counter << endl;
	
	}while(Victory_Detect(grid) != 1);
	
	Winner_declaration(turn_counter);
}

int BoardInit(int Grid[6][7]){ //initiate the board
	for(int i = 0 ; i < 6 ; i++){
		for(int j = 0 ; j < 7 ; j++){
			Grid[i][j] = 0;
		}
	}
	return 0;
}

int PlayerInput(){ // player choose which collumn
	int player;
	cout << "Collumn : " ;
	do{
		cin >> player;
		if(player > 7){
		cout << "That's Collumn doesn't exit baka!" << endl;
		}
		else return player;
	}while(player > 7);
}

void Display(int Grid[6][7] ){	//for display the board after player inputing
	for(int i = 0 ; i < 6 ; i ++){
		for(int j = 0 ; j < 7 ; j++){
			if(Grid[i][j] == 0){
			cout << " " << " | ";
			}else if(Grid[i][j] == 1){
				cout << "X" << " | ";
			}else if(Grid[i][j] == 2){
				cout << "O" << " | ";
			}
		}
	cout <<endl << "--------------------------" << endl;
	}
}

void GridUpdate(int Grid[6][7] , int player , int which_player){//update the grid
	for(int i = 5 ; i >= 0 ; i--){
		if(Grid[i][player] == 0 && which_player == 1){
			Grid[i][player] = 1;
			break;
		}else if(Grid[i][player] == 0 && which_player ==2){
			Grid[i][player] = 2;
			break;
		}  
	}
}

int turn(int &turn_counter){ // turn checking
	if(turn_counter%2 == 1){
		return 1;
	}
	if(turn_counter%2 == 0){
		return 2;
	}
}

int Victory_Detect(int Grid[6][7]){
	for(int i = 0 ; i < 6 ; i++){
		for( int j = 0 ; j < 7 ; j++){
			if(Grid[i][j] == 1 || Grid[i][j] == 2){
				if( Grid[i][j] == Grid[i][j+1]){	//Horiziontal Winning dectection
					if( Grid[i][j] == Grid[i][j+2]){
						if(Grid[i][j] == Grid[i][j+3]){
								return 1;
						}
					}
				}
				
			else if( Grid[i][j] == Grid[i][j-1]){	//Horiziontal winning Dectection
					if( Grid[i][j] == Grid[i][j-2]){
						if(Grid[i][j] == Grid[i][j-3]){
								return 1;
						}
					}   
				}
			else if( Grid[i][j] == Grid[i+1][j]){	//Vertical winning Detection
					if( Grid[i][j] == Grid[i+2][j]){
						if(Grid[i][j] == Grid[i+3][j]){
								return 1;
						}
					}
				}
			else if( Grid[i][j] == Grid[i-1][j]){	//Vertical winning Detection
					if( Grid[i][j] == Grid[i-2][j]){
						if(Grid[i][j] == Grid[i-3][j]){
								return 1;
						}
					}
				}
			else if( Grid[i][j] == Grid[i-1][j+1]){	//Diaxial winnning Detection
					if( Grid[i][j] == Grid[i-2][j+2]){
						if(Grid[i][j] == Grid[i-3][j+3]){
								return 1;
						}
					}
				}
			else if( Grid[i][j] == Grid[i+1][j-1]){	//Diaxial winning Detection
					if( Grid[i][j] == Grid[i+2][j-2]){
						if(Grid[i][j] == Grid[i+3][j-3]){
								return 1;
						}
					}
				}
			else if( Grid[i][j] == Grid[i+1][j+1]){	//Diaxial winning Detection
					if( Grid[i][j] == Grid[i+2][j+2]){
						if(Grid[i][j] == Grid[i+3][j+3]){
								return 1;
						}
					}
				}
			else if( Grid[i][j] == Grid[i-1][j-1]){ //Diaxial winning Detection
					if( Grid[i][j] == Grid[i-2][j-2]){
						if(Grid[i][j] == Grid[i-3][j-3]){
								return 1;
						}
					}
				}
			}
		}
	}
}

void Winner_declaration(int player){
	int winner = 0;
	winner = player % 2;
	if(winner == 1){
		cout << "Player 1 win!";
	}else if(winner == 0){
		cout << "Player 2 win!";
	}
}


